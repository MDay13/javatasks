/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MDay
 */

@XmlRootElement(name = "students")
@XmlAccessorType (XmlAccessType.FIELD)
public class Students 
{
    @XmlElement(name = "student")
    private List<Student> students;

    public List<Student> getStudents() 
    {
        return students;
    }

    public void setStudents(List<Student> students) 
    {
        this.students = students;
    }
    
    public void addStudent (Student student)
    {   if (students==null)
            students=new ArrayList<>();
        students.add(student);
    }
}
