/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBException;


/**
 *
 * @author MDay
 */
public class XMLExample {

    public static void main(String argv[]) throws JAXBException 
    {
        Students students;
        File file = new File("students.xml");
        
        students = DOMParser.parse(file);
       // DOMParser.addStudent(file, new Student(2,"Petya", "Vasya", 12));
        
        students = MySaxParser.parse(file);
        
        students = MyJAXBParser.parse(file);
        
        MyJAXBParser.addStudent(new Student(99, "NEW", "TRUE", 88), file);
        
        System.out.println(students);
    }

}

    
