/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.io.File;

/**
 *
 * @author MDay
 */
public interface MyXMLParser 
{
    public Students parse(File file);
    public void addStudent(Student student, File file);
}
