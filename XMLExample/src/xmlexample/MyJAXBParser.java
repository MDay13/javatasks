/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author MDay
 */
public class MyJAXBParser
{
    public static Students parse(File file) throws JAXBException
    {
        JAXBContext jaxbContext = JAXBContext.newInstance(Students.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        
        Students students = (Students) jaxbUnmarshaller.unmarshal(file);

        return students;
    }
    
    public static void addStudent(Student addedStudent, File file) throws JAXBException
    {
        Students students = parse(file);
        JAXBContext jaxbContext = JAXBContext.newInstance(Students.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(students, file);
    }
}
