/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.util.ArrayList;
import java.util.List;
 
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
 
 /**
 *
 * @author MDay
 */
 
public class MyHandler extends DefaultHandler {
 
    //List to hold Employees object
    private List<Student> students = null;
    private Student student = null;
 
 
    //getter method for employee list
    public List<Student> getStudents() 
    {
        return students;
    }
 
 
    boolean bfName = false;
    boolean blName = false;
    boolean bMarks = false;
 
 
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
 
        if (qName.equalsIgnoreCase("student")) 
        {
            String id = attributes.getValue("id");
            student = new Student();
            student.setId(Integer.parseInt(id));
            //initialize list
            if (students == null)
                students = new ArrayList<>();
        } 
        else if (qName.equalsIgnoreCase("firstName")) 
        {
            bfName = true;
        } 
        else if (qName.equalsIgnoreCase("lastName")) 
        {
            blName = true;
        }
        else if (qName.equalsIgnoreCase("marks")) 
        {
            bMarks = true;
        }
    }
 
 
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("student")) {
            //add Employee object to list
            students.add(student);
        }
    }
 
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
 
        if (bMarks) 
        {
            student.setMarks(Integer.parseInt(new String(ch, start, length)));
            bMarks = false;
        } 
        else if (bfName) 
        {
            student.setFirstName(new String(ch, start, length));
            bfName = false;
        } 
        else if (blName) 
        {
            student.setLastName(new String(ch, start, length));
            blName = false;
        } 
    }
}