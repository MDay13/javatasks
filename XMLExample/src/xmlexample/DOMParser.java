/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author MDay
 */
public class DOMParser
{
    public static Students parse(File file)
    {
        try 
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
             doc.getDocumentElement().normalize();

             NodeList nList = doc.getElementsByTagName("student");

             Students students = new Students();
             
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   Integer id = Integer.parseInt(eElement.getAttribute("id"));
                   String firstname =   eElement
                        .getElementsByTagName("firstname")
                        .item(0)
                        .getTextContent();
                   String lastname =   eElement
                      .getElementsByTagName("lastname")
                      .item(0)
                      .getTextContent();
                   Integer marks =  Integer.parseInt(eElement
                      .getElementsByTagName("marks")
                      .item(0)
                      .getTextContent());
                   
                   students.addStudent(new Student(id, firstname, lastname, marks));
                }           
            }
             
             return students;
        } 
        catch (Exception e) 
        {
         e.printStackTrace();
        }
        
        return null;
    }

    public static void addStudent(Student addedStudent, File file)
    {
         try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.parse(file);
		Element rootElement = doc.getDocumentElement();
                //doc.appendChild(rootElement);

		// staff elements
		Element student = doc.createElement("student");
		rootElement.appendChild(student);

		// set attribute to staff element
		Attr attr = doc.createAttribute("id");
		attr.setValue(addedStudent.getId().toString());
		student.setAttributeNode(attr);

		// shorten way
		// staff.setAttribute("id", "1");

		// firstname elements
		Element firstname = doc.createElement("firstname");
		firstname.appendChild(doc.createTextNode(addedStudent.getFirstName()));
		student.appendChild(firstname);

		// lastname elements
		Element lastname = doc.createElement("lastname");
		lastname.appendChild(doc.createTextNode(addedStudent.getLastName()));
		student.appendChild(lastname);

		// salary elements
		Element salary = doc.createElement("marks");
		salary.appendChild(doc.createTextNode(addedStudent.getMarks().toString()));
		student.appendChild(salary);

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(file);

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);

		System.out.println("File saved!");

	  } catch (Exception ex) 
          {
            ex.printStackTrace();
          }

    }
}
