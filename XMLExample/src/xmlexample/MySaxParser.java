/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlexample;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author MDay
 */
public class MySaxParser
{
    public static Students parse(File file)
    {
        List<Student> students = null;
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try 
        {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            MyHandler handler = new MyHandler();
            saxParser.parse(file, handler);
            students = handler.getStudents();
        } 
        catch (ParserConfigurationException | SAXException | IOException e) 
        {
            e.printStackTrace();
        }
        Students result = new Students();
        result.setStudents(students);
        return result;
    }
}
