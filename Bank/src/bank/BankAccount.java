/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author MDay
 */
class BankAccount 
{
    private volatile int balance;

    public BankAccount(int balance)
    {
        this.balance = balance;
    }

    public BankAccount()
    {
        balance = 0;
    }

    synchronized public int getBalance()
    {
        return balance;
    }
    
    synchronized public int withdraw(int sum)
    {
        if(sum <= getBalance())
        {
            balance = balance-sum;
            return sum;
        }
        else
        {
            return 0;
        }
    }
}
