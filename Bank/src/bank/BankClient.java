/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.util.Random;


/**
 *
 * @author MDay
 */
public class BankClient extends Thread
{
    BankAccount bankAccount;
    private int sum;
    private int time;

    public BankClient(BankAccount bankAccount) 
    {
        this.bankAccount = bankAccount;
        this.sum = new Random().nextInt(200)-100;
        this.time = new Random().nextInt(3000)+2000;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
    
}
