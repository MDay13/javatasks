/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author MDay
 */
public class Bank {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        BankAccount account = new BankAccount(250);
        System.out.println("Init balance: " + account.getBalance());
        ArrayList<Operator> operators = new ArrayList<>();
        for (int i=0;i<3;i++)
        {
            System.out.println("Operator "+i+" started..");
            Operator operator = new Operator("Operator "+i,account);
            operator.start();
            operators.add(operator);
        }
        BankClientGenerator bcg = new BankClientGenerator(operators, account);
        bcg.start();
    }
    
}
