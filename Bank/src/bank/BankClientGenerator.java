/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author MDay
 */
public class BankClientGenerator extends Thread
{
    private final BankAccount account;
    
    private ArrayList<Operator> operators;

    public BankClientGenerator(ArrayList<Operator> operators, BankAccount account) 
    {
        this.operators = operators;
        this.account = account;
    }

    @Override
    public void run() {
        for(;;) 
        {
            Operator operator = operators.get(0);
            for (int i = 0; i < 10; i++) 
            {
                BankClient client = new BankClient(account);
                for (Operator currentOperator : operators)
                {
                    if (currentOperator.getClients().size() < operator.getClients().size()) 
                    {
                        operator = currentOperator;
                    }
                }
                try 
                {
                    operator.getClients().put(client);
                    Thread.sleep((new Random().nextInt(1000)));
                } 
                catch (Exception e) 
                {
                    e.printStackTrace();
                }

            }
        }
    }
    
}
