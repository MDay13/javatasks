/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author MDay
 */
public class Operator extends Thread
{
    private final String name;
    private BlockingQueue<BankClient> clients = new LinkedBlockingQueue();
    private BankAccount account;
    
    public Operator(String name, BankAccount account) {
        this.name = name;
        this.account = account;
    }

    public BlockingQueue<BankClient> getClients() {
        return clients;
    }

    public void setClients(BlockingQueue<BankClient> clients) {
        this.clients = clients;
    }
    
    @Override
    public void run ()
    {
        for (;;)
        {
            try
            {
                BankClient client = clients.take();
                if (account.getBalance()>client.getSum())
                {
                    int sum = account.withdraw(client.getSum());
                    
                    System.out.println("-----------------------------");
                    System.out.println("Client:"+client.getName());
                    System.out.println("Sum:" + sum);
                    System.out.println("Balance:" + account.getBalance());
                }
                else
                {
                    while (account.getBalance() < client.getSum() && client.getTime() > 0) 
                    {
                        Thread.sleep(100);
                        client.setTime(client.getTime() - 100);
                    }
                    
                    System.out.println("Hasn't money for: " + client.getName());
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
