/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bankthread;
/**
 *
 * @author titarenko_n_yu
 */
public  class BankAccount {
     private static int balance;

    public BankAccount() {
        this.balance =50000;
    }

    public  BankAccount(int balance) {
        this.balance = balance;
    }

    public static int getBalance() {
        return balance;
    }

    public static synchronized boolean takeMoney (int money) 
    {
        boolean s = false;    
        if(getBalance()>=money)
        {
            s = true;
            balance -= money;
            System.out.println("Take money: " + money);
            System.out.println("Balance:  " + getBalance());
        }
        return s;
    }
    public static synchronized void depositMoney (int money) 
    {
        balance += money;
        System.out.println("Deposit: " + money);
        System.out.println("Balance:  " + getBalance());
    }
}
