/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankthread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author titarenko_n_yu
 */
public class Operator implements Runnable {

    private final String name;
    private BlockingQueue<Client> clients = new LinkedBlockingQueue();
    
    public Operator(String name ) {
        this.name = name;
    }

    public BlockingQueue<Client> getClients() 
    {
        return clients;
    }
   
    @Override
    public void run() 
    {
        while(true)
        {
            try 
            {
                Client client = clients.take();
                if (client.getOperation() == 0) 
                {
                    if (BankAccount.getBalance() >= client.getSum()) 
                    {
                        System.out.println(name + ": client " + client.getName());
                        BankAccount.takeMoney(client.getSum());

                        Thread.sleep(client.getTime());
                    } 
                    else 
                    {
                        while (BankAccount.getBalance() < client.getSum() && client.getTime() > 0) 
                        {
                            client.setTime(client.getTime() - 1);
                        }
                        System.out.println("No  money for: " + client.getName());
                    }
                } 
                else 
                {
                    System.out.println(name + ": client " + client.getName());
                    BankAccount.depositMoney(client.getSum()); // положить
                    System.out.println("");
                    Thread.sleep(client.getTime());
                }
            } 
            catch (InterruptedException ex) 
            {
                Logger.getLogger(Operator.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
    }
}
