/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankthread;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author titarenko_n_yu
 */
public class ClientGenerator implements Runnable {

    private ArrayList<Operator> operators;

    public ClientGenerator(ArrayList<Operator> operators) 
    {
        this.operators = operators;
    }

    @Override
    public void run() {
        while (true) 
        {
            Operator operator = operators.get(0);
            for (int i = 0; i < 10; i++) {
                Client client = new Client();
                for (Operator oper : operators)
                {
                    if (oper.getClients().size() < operator.getClients().size()) 
                    {
                        operator = oper;
                    }
                }
                try 
                {
                    operator.getClients().put(client);
                } 
                catch (InterruptedException ex) 
                {
                    Logger.getLogger(ClientGenerator.class.getName()).log(Level.SEVERE, null, ex);
                }
                try 
                {
                    Thread.sleep((new Random().nextInt(1000)));
                } catch (InterruptedException e) 
                {
                    e.printStackTrace();
                }

            }
        }
    }
}
