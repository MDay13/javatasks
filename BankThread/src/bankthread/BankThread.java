/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bankthread;

import java.util.ArrayList;

/**
 *
 * @author titarenko_n_yu
 */
public class BankThread {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BankAccount bankAccount = new BankAccount(500);
        ArrayList<Operator> operators = new ArrayList<>();
        operators.add(new Operator("Operator 1"));
        operators.add(new Operator("Operator 2"));
        operators.add(new Operator("Operator 3"));
        for (int i = 0; i < operators.size(); i++) 
        {       
            Thread threadOperator = new Thread(operators.get(i));
            threadOperator.start();
        }
        ClientGenerator clients = new ClientGenerator(operators);
        new Thread(clients).start();       
    }
    
}
