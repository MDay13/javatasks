/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bankthread;

import java.sql.Time;
import java.util.Random;

/**
 *
 * @author titarenko_n_yu
 */
public class Client {
    private String name;
    private int sum;
    private int time;
    private int operation;

    public Client(){
        this.name = "client "+(new Random().nextInt(1000)) ;
        sum = 1+new Random().nextInt(100);
        operation = new Random().nextInt(2);
        time = new Random().nextInt(1000);
    }
    
    public int getOperation() {
        return operation;
    }

    public String getName() {
        return name;
    }

    public int getSum() {
        return sum;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

}
